FROM python:3.9.4-buster AS builder

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y fonts-noto-core \
    && mv /usr/share/fonts/truetype /tmp/ \
    && mkdir -p /usr/share/fonts/truetype/noto \
    && mv /tmp/truetype/noto/NotoSansMono-Bold.ttf /usr/share/fonts/truetype/noto/

RUN python -m venv venv \
    && venv/bin/pip3 install --no-cache-dir wheel \
    && venv/bin/pip3 install --no-cache-dir --upgrade python-telegram-bot==12.0.0b1 \
    && venv/bin/pip3 install --no-cache-dir requests beautifulsoup4 pyqrcode Image pypng Pillow emoji apscheduler

FROM python:3.9.4-slim-buster

WORKDIR /usr/src/app
COPY --from=builder /usr/src/app .
COPY --from=builder /usr/share/fonts /usr/share/fonts
COPY . .
# TODO pull this from var
COPY --from=registry.gitlab.com/pandacoin/pandacoin/pandacoind:v4-9-0 /usr/local/bin /usr/local/bin

# TODO main net support / make this not static
RUN mv data / \
    && mkdir -p /root/.pandacoin
COPY pandacoin.conf /root/.pandacoin/

ENV DATADIR /data
VOLUME /data
CMD /usr/src/app/venv/bin/python /usr/src/app/tipbot.py
